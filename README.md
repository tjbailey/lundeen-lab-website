# Lundeen Lab Website Update Project

## Introduction

We are using the Wowchemy theme for Hugo to build the website.

https://wowchemy.com/docs/

https://gohugo.io/documentation/


The website is hosted with netlify. This automatically builds an updated website whenever the main branch of the git repository is updated.

Currently the website is found at https://lundeenlab.netlify.app (the domain is very easy to switch once the website is finished). The admin control pannel is at https://lundeenlab.netlify.app/admin.


## Local Build
You can locally build and test the website if you have hugo installed. The installation instructions are found [here](https://gohugo.io/getting-started/installing/). The latest hugo version can be found at https://github.com/gohugoio/hugo/releases. Note: to build the website locally with wowchemy, you also need to have go installed (why can be found [here](https://wowchemy.com/docs/hugo-tutorials/troubleshooting/#error-go-executable-not-found) and you will also need go installed found [here](https://go.dev/doc/install)). The wowchemy theme does make use of additional features and so you will need to install the extend version (eg. hugo_extended_0.96.0_Windows-64bit.zip).

To build the website locally you then just run the command `hugo server` in the website root folder. This will then run the website on a local webserver that you can then open.


## Editing Hugo Pages
Hugo makes use of a widget based structure and [markdown](https://www.markdownguide.org/cheat-sheet/) for formatting. In general how this works is that each page has its own folder and then any widgets have a seperate `.md` file defining them and their content (this should make some sense of the files already here). The order of widgets is determined by their weight. A list of widgets and their details can be found https://wowchemy.com/docs/widget/.

Every page will have a index.md file to define the page (most just to define it is a widget). Pages can be linked from the top menu in the `_default/menus.yaml` file.

A special type of page is the of group members (found in the `authors` folder). Each folder should contain the index file that describes the group member and an image `avatar.jpg`. These authors can be used if we were writing blogs/articles (probably not relevant) and in the people page.

General website settings are found in the `_default` folder.

## Building Static Backup
To update the static backup contained in the `public` directory run the `hugo` command.