---
# Display name
title: Jeff Lundeen

# Username (this should match the folder name)
authors:
  #- admin
  - jeff-s.-lundeen

# Is this the primary user of the site?
superuser: true

# Role/position
role: Professor of Physics

# Organizations/Affiliations
organizations:
  - name: University of Ottawa
    url: ''

# Short bio (displayed in user profile at end of posts)
# bio: 

# List each interest with a dash
# interests:
# - Interest 1
# - Interest 2

# education:
#   courses:
#   - course: Title course 1
#     institution: Name of Institution
#     year: 2012
#   - course: Title course 1
#     institution: Name of Institution
#     year: 2012

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'jeff.lundeen@gmail.com'
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/LundeenOttawa
  - icon: google-scholar
    icon_pack: ai
    link: https://scholar.google.com/citations?user=utTnl-QAAAAJ
  # - icon: github
  #   icon_pack: fab
  #   link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
  - icon: cv
    icon_pack: ai
    link: files/JeffLundeenCV.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ''

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
  - Group Leader
---

I am a Canada Research Chair in Quantum Photonics and an Assistant Professor in the Physics Dept. of the University of Ottawa. Prior to July 2013, I was a permanent Researcher (Associate Research Officer) at the National Research Council (NRC) of Canada. I was born in Toronto, Canada. I did my undergraduate degree at Queen's University in Kingston, Ontario. After, I returned to Toronto to work with Dr. Aephraim Steinberg at the University of Toronto where I earned my Masters and Ph.D. in experimental quantum optics and  quantum information.  As a Postdoctoral Fellow, I then did experimental research in the group of Prof. Ian Walmsely at the Clarendon Laboratory, University of Oxford. After a brief stint at ICFO in Barcelona working with Morgan Mitchell, I moved to Ottawa.
