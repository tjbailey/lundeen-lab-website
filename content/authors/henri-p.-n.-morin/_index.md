---
# Display name
title: "Henri Morin"

# Username (this should match the folder name and the name on publications)
authors:
- "henri-p.-n.-morin"

# Is this the primary user of the site?
superuser: false

# Role/position (e.g., Professor of Artificial Intelligence)
role: PhD Studies University of Waterloo

# Organizations/Affiliations
# organizations:
# - name: 
#   url: ""

# Short bio (displayed in user profile at end of posts)
# bio: 

# List each interest with a dash
# interests:
# - Interest 1
# - Interest 2

# education:
#   courses:
#   - course: Title course 1
#     institution: Name of Institution
#     year: 2012
#   - course: Title course 1
#     institution: Name of Institution
#     year: 2012

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/hpnmorin
# - icon: github
#   icon_pack: fab
#   link: https://github.com/hpnmorin
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Alumni
---
Henri originally joined the LundeenLab in 2018, spending eight months as a co-op student, before re-joining the group in 2020 as a MSc student in the Department of Physics. He attended uOttawa where he completed both his Honours BSc in Physics-Mathematics (co-op) and MSc in Physics. His research includes integrated photonics and a novel application of nonlinear optics. Outside of research, he enjoys curling in the winter, reading, and video games. 