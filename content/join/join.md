---
widget: blank
weight: 10
headless: true

title: Join the group!

design:
 columns: "1"


---

## Available Positions

Please [email](../contact) Jeff Lundeen directly to enquire. Useful information to attach to your email includes your CV, transcript (unofficial is okay), and up to three publications (if you have any). Consult [here](https://www2.uottawa.ca/study/graduate-studies) for more information about grad studies at uOttawa.

We are currently hiring undergraduate summer researchers, co-op students, Master's students, PhD students, and postdocs. 

## Why Quantum Photonics?

Quantum Photonics is an ideal field in which to do a graduate degree because it directly links the fundamental with the applied. For example, students in Quantum Photonics get training in applied subjects such as building, designing and operating lasers, modeling optical microchips, fibre optics, high-speed electronics, optical raytracing, and nonlinear optics. On the other hand, we use these tools and techniques to build devices and experiments that use and test the underlying ideas and concepts from quantum physics like entanglement and measurement.

There is no other field in which you can personally build your own experiment from the ground up and yet still probe some of the central questions we have about the world like "what is a quantum wavefunction?". During the course of their degree, students in quantum photonics typically build and perform a number of  experiments. These self-contained exeriments usually fit on a single optical table which itself takes up roughly a third of a laboratory. In the process, students are trained and supported by other members of our group with goal of  becoming independent and accomplished researchers

## Why the University of Ottawa?

Founded in 1848, the [University of Ottawa](https://www2.uottawa.ca/en) is a large French and English institution located in downtown Ottawa along the Rideau Canal.

## Why Ottawa?

Lorem ipsum

## Living in Ottawa

Ottawa is the Capital of Canada and is the fourth largest city in the country. Ottawa is home to many of Canada's high quality National Museums and Cultural Institutions such as the [National Gallery of Canada](https://www.gallery.ca/), the [Museum of Nature](https://nature.ca/en/), and the [National Arts Centre](https://nac-cna.ca/en/).

Ottawa has plenty of natural space since it is positioned at the meeting of four significant waterways: the Gatineau River, the Rideau River, the Rideau Canal, and the Ottawa river. The latter is one of the world's great rivers and is home to siling, fishing, canoeing, and swimming right in the middle of the city. Every winter, the Rideau Canal freezes over and is turned into the world's largest (almost 8km long) skating rink. Stretching right into the Ottawa area, [Gatineau Park](https://en.wikipedia.org/wiki/Gatineau_Park) is a vast 360 square kilometer natural park with camping, cross-country skiing, hiking, down-hill skiing, snow-shoeing, swimming, canoeing and more.