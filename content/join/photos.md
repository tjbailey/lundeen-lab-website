---
widget: slider  # Use the Slider widget as this page section
weight: 20  # Position of this section on the page
active: true  # Publish this section?
headless: true  # This file represents a page section.

design:
  # Slide height is automatic unless you force a specific height (e.g. '400px')
  slide_height: ''
  is_fullscreen: true
  # Automatically transition through slides?
  loop: true
  # Duration of transition between slides (in ms)
  interval: 10000

content:
  slides:
    - title: 👋 Welcome to the group
      # content: Take a look at what we're working on...
      align: center
      background:
        position: center
        color: '#666'
        brightness: 0.7
        media: group_picture_2022.jpg
        fit: cover
      link:
        icon: graduation-cap
        icon_pack: fas
        text: Join Us
        url: ../contact/
    - title: '' #Hologram
      # content: 'Share your knowledge with the group and explore exciting new topics together!'
      align: center
      background:
        position: center
        color: '#555'
        brightness: 0.7
        media: lab_logo_hologram.jpg
        fit: cover
    - title: '' # Experiment
      # content: Take a look at what we're working on...
      align: center
      background:
        position: center
        color: '#666'
        brightness: 0.7
        media: mba.jpg
        fit: cover
---
