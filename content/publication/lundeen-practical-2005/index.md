---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Practical measurement of joint weak values and their connection to the annihilation
  operator
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- K. J. Resch
tags:
- Annihilation operator
- Entanglement
- Post-selection
- Weak measurement
categories: []
date: '2005-01-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:10.058548Z'
publication_types:
- '2'
abstract: Weak measurements are a new tool for characterizing post-selected quantum
  systems during their evolution. Weak measurement was originally formulated in terms
  of von Neumann interactions which are practically available for only the simplest
  single-particle observables. In the present work, we extend and greatly simplify
  a recent, experimentally feasible, reformulation of weak measurement for multiparticle
  observables [Phys. Rev. Lett. 92 (2004) 130402]. We also show that the resulting
  “joint weak values” take on a particularly elegant form when expressed in terms
  of annihilation operators.
publication: '*Physics Letters A*'
doi: 10.1016/j.physleta.2004.11.037
links:
- name: URL
  url: https://www.sciencedirect.com/science/article/pii/S0375960104016342
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0501072
- name: PDF
  url: files/publications/lundeen-practical-2005.pdf
---
