---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Determining complementary properties using weak-measurement: uncertainty,
  predictability, and disturbance'
subtitle: ''
summary: ''
authors:
- G. S. Thekkadath
- F. Hufnagel
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-11-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.906933Z'
publication_types:
- '2'
abstract: It is often said that measuring a system’s position must disturb the complementary
  property, momentum, by some minimum amount due to the Heisenberg uncertainty principle.
  Using a ‘weak-measurement’, this disturbance can be reduced. One might expect this
  comes at the cost of also reducing the measurement’s precision. However, it was
  recently demonstrated that a sequence consisting of a weak position measurement
  followed by a regular momentum measurement can probe a quantum system at a single
  point, with zero width, in position-momentum space. Here, we study this ‘joint weak-measurement’
  and reconcile its compatibility with the uncertainty principle. While a single trial
  probes the system with a resolution that can saturate Heisenberg’s limit, we show
  that averaging over many trials can be used to surpass this limit. The weak-measurement
  does not trade away precision, but rather another type of uncertainty called ‘predictability’
  which quantifies the certainty of retrodicting the measurement’s outcome.
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/aaecdf
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/aaecdf
- name: arXiv
  url: https://arxiv.org/abs/1809.05984
- name: PDF
  url: files/publications/thekkadath-determining-2018.pdf
---
