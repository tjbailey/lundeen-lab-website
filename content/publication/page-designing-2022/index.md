---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Designing high-performance propagation-compressing spaceplates using thin-film
  multilayer stacks
subtitle: ''
summary: ''
authors:
- Jordan T. R. Pagé
- Jordan T. R. Pagé
- Orad Reshef
- Robert W. Boyd
- Jeff S. Lundeen
tags: []
categories: []
date: '2022-01-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:03.711427Z'
publication_types:
- '2'
abstract: The development of metasurfaces has enabled unprecedented portability and
  functionality in flat optical devices. Spaceplates have recently been introduced
  as a complementary element to reduce the space between individual metalenses, which
  will further miniaturize entire imaging devices. However, spaceplates necessitate
  an optical response which depends on the transverse spatial frequency component
  of a light field &#x2014; therefore making it challenging both to design them and
  to assess their ultimate performance and potential. Here, we employ inverse-design
  techniques to explore the behaviour of general thin-film-based spaceplates. We observe
  a tradeoff between the compression factor R and the numerical aperture NA of such
  devices; we obtained a compression factor of R=5.5 for devices with an NA&#x2009;&#x003D;&#x2009;0.42,
  and up to a record R=340 with NA of 0.017. Our work illustrates that even simple
  designs consisting of realistic materials (i.e., silicon and glass) permit capable
  spaceplates for monochromatic applications.
publication: '*Optics Express*'
doi: 10.1364/OE.443067
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-30-2-2197
- name: arXiv
  url: https://arxiv.org/abs/2106.12106
- name: PDF
  url: files/publications/page-designing-2022.pdf
---
