---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Direct Measurement of the Density Matrix of a Quantum System
subtitle: ''
summary: ''
authors:
- G. S. Thekkadath
- Lambert Giner
- Y. Chalich
- M. J. Horton
- J. Banker
- Jeff S. Lundeen
tags: []
categories: []
date: '2016-09-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.118970Z'
publication_types:
- '2'
abstract: One drawback of conventional quantum state tomography is that it does not
  readily provide access to single density matrix elements since it requires a global
  reconstruction. Here, we experimentally demonstrate a scheme that can be used to
  directly measure individual density matrix elements of general quantum states. The
  scheme relies on measuring a sequence of three observables, each complementary to
  the last. The first two measurements are made weak to minimize the disturbance they
  cause to the state, while the final measurement is strong. We perform this joint
  measurement on polarized photons in pure and mixed states to directly measure their
  density matrix. The weak measurements are achieved using two walk-off crystals,
  each inducing a polarization-dependent spatial shift that couples the spatial and
  polarization degrees of freedom of the photons. This direct measurement method provides
  an operational meaning to the density matrix and promises to be especially useful
  for large dimensional states.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.117.120401
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.117.120401
- name: arXiv
  url: https://arxiv.org/abs/1604.07917
- name: PDF
  url: files/publications/thekkadath-direct-2016.pdf
---
