---
# Documentation: https://wowchemy.com/docs/managing-content/

title: High-dimension experimental tomography of a path-encoded photon quantum state
subtitle: ''
summary: ''
authors:
- Davor Curic
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2019-07-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.626502Z'
publication_types:
- '2'
abstract: Quantum information protocols often rely on tomographic techniques to determine
  the state of the system. A popular method of encoding information is on the different
  paths a photon may take, e.g., parallel waveguides in integrated optics. However,
  reconstruction of states encoded onto a large number of paths is often prohibitively
  resource intensive and requires complicated experimental setups. Addressing this,
  we present a simple method for determining the state of a photon in a superposition
  of d paths using a rotating one-dimensional optical Fourier transform. We establish
  the theory and experimentally demonstrate the technique by measuring a wide variety
  of six-dimensional density matrices. The average fidelity of these with the expected
  state is as high as 0.9852&#x00B1;0.0008. This performance is comparable to or exceeds
  established tomographic methods for other types of systems.
publication: '*Photonics Research*'
doi: 10.1364/PRJ.7.000A27
links:
- name: URL
  url: https://opg.optica.org/prj/abstract.cfm?uri=prj-7-7-A27
- name: arXiv
  url: https://arxiv.org/abs/1906.04122
- name: PDF
  url: files/publications/curic-high-dimension-2019.pdf

---
