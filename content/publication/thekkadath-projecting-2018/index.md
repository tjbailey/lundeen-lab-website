---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Projecting onto any two-photon polarization state using linear optics
subtitle: ''
summary: ''
authors:
- G. S. Thekkadath
- Lambert Giner
- X. Ma
- Jefferson Flórez
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-08-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.019911Z'
publication_types:
- '2'
abstract: Projectors are a simple but powerful tool for manipulating and probing quantum
  systems. For instance, projecting two-qubit systems onto maximally entangled states
  can enable quantum teleportation. While such projectors have been extensively studied,
  partially-entangling projectors have been largely overlooked, especially experimentally,
  despite their important role in quantum foundations and quantum information. Here,
  we propose a way to project two polarized photons onto any state with a single experimental
  setup. Our scheme does not require optical nonlinearities or additional photons.
  Instead, the entangling operation is provided by Hong–Ou–Mandel interference and
  post-selection. The efficiency of the scheme is between 50% and 100%, depending
  on the projector. We perform an experimental demonstration and reconstruct the operator
  describing our measurement using detector tomography. Finally, we flip the usual
  role of measurement and state in Hardy’s test by performing a partially-entangling
  projector on separable states. The results verify the entangling nature of our measurement
  with six standard deviations of confidence.
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/aad9b9
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/aad9b9
- name: arXiv
  url: https://arxiv.org/abs/1805.03753
- name: PDF
  url: files/publications/thekkadath-projecting-2018.pdf
---
