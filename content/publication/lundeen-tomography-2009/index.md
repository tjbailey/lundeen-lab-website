---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Tomography of quantum detectors
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- A. Feito
- H. Coldenstrodt-Ronge
- K. L. Pregnell
- Ch Silberhorn
- T. C. Ralph
- J. Eisert
- M. B. Plenio
- I. A. Walmsley
tags:
- Atomic
- Classical and Continuum Physics
- Complex Systems
- Condensed Matter Physics
- general
- Mathematical and Computational Physics
- Molecular
- Optical and Plasma Physics
- Physics
- Theoretical
categories: []
date: '2009-01-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.868915Z'
publication_types:
- '2'
abstract: In quantum mechanics, measurement has a fundamentally different role than
  in classical physics. Now a general method has been devised to characterize a quantum
  measurement device, completing the suite of so-called tomography techniques required
  to fully specify an experiment.
publication: '*Nature Physics*'
doi: 10.1038/nphys1133
links:
- name: URL
  url: https://www.nature.com/articles/nphys1133
- name: arXiv
  url: https://arxiv.org/abs/0807.2444
- name: PDF
  url: files/publications/lundeen-tomography-2009.pdf
---
