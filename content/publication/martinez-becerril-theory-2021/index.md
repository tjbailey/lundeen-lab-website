---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Theory and experiment for resource-efficient joint weak-measurement
subtitle: ''
summary: ''
authors:
- Aldo C. Martinez-Becerril
- Gabriel Bussières
- Davor Curic
- Lambert Giner
- Raphael A. Abrahao
- Jeff S. Lundeen
tags: []
categories: []
date: '2021-12-01'
lastmod: 2022-10-12T19:54:33-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:54:33.785721Z'
publication_types:
- '2'
abstract: Incompatible observables underlie pillars of quantum physics such as contextuality
  and entanglement. The Heisenberg uncertainty principle is a fundamental limitation
  on the measurement of the product of incompatible observables, a 'joint' measurement.
  However, recently a method using weak measurement has experimentally demonstrated
  joint measurement. This method [Lundeen, J. S., and Bamber, C. Phys. Rev. Lett.
  108, 070402, 2012] delivers the standard expectation value of the product of observables,
  even if they are incompatible. A drawback of this method is that it requires coupling
  each observable to a distinct degree of freedom (DOF), i.e., a disjoint Hilbert
  space. Typically, this 'read-out' system is an unused internal DOF of the measured
  particle. Unfortunately, one quickly runs out of internal DOFs, which limits the
  number of observables and types of measurements one can make. To address this limitation,
  we propose and experimentally demonstrate a technique to perform a joint weak-measurement
  of two incompatible observables using only one DOF as a read-out system. We apply
  our scheme to directly measure the density matrix of photon polarization states.
publication: '*Quantum*'
doi: 10.22331/q-2021-12-06-599
links:
- name: URL
  url: https://quantum-journal.org/papers/q-2021-12-06-599/
- name: arXiv
  url: https://arxiv.org/abs/2103.16389
- name: PDF
  url: files/publications/martinez-becerril-theory-2021.pdf
---
