---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Quantum-interferometrically enhanced optical nonlinearities for photon switches
  and logic gates
subtitle: ''
summary: ''
authors:
- Aephraim Steinberg
- Jeff S. Lundeen
- Kevin Resch
tags:
- beams
- phase
- photon
- pump
- quantum
categories: []
date: '2003-07-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.145371Z'
publication_types:
- '8'
abstract: The present invention shows that all-optical switching (nonlinearity) may
  be enhanced by huge factors (e.g., ten orders of magnitude), making it possible
  for beams of light to control one another even In the extreme low-light-level regime
  (down to mean photon numbers smaller than 1). Such photon switches constitute novel
  quantum optical logic gates which may enable new technologies in quantum information
  processing as well as other low-light-level optical devices. The present invention
  also provides a device which greatly enhances nonlinear optical effects between
  photon pairs in input laser beams via quantum interference. The device is capable
  of removing all (or nearly all) photon pairs from the input beams, efficiently converting
  them to their sum frequency.
publication: ''
links:
- name: URL
  url: https://patents.google.com/patent/US20030123516A1/en
---
