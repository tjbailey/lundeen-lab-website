---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Measuring measurement: theory and practice'
subtitle: ''
summary: ''
authors:
- A. Feito
- Jeff S. Lundeen
- H. Coldenstrodt-Ronge
- J. Eisert
- M. B. Plenio
- I. A. Walmsley
tags: []
categories: []
date: '2009-09-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.801659Z'
publication_types:
- '2'
abstract: Recent efforts have applied quantum tomography techniques to the calibration
  and characterization of complex quantum detectors using minimal assumptions. In
  this work, we provide detail and insight concerning the formalism, the experimental
  and theoretical challenges and the scope of these tomographical tools. Our focus
  is on the detection of photons with avalanche photodiodes and photon-number resolving
  detectors and our approach is to fully characterize the quantum operators describing
  these detectors with a minimal set of well-specified assumptions. The formalism
  is completely general and can be applied to a wide range of detectors.
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/11/9/093038
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/11/9/093038
- name: arXiv
  url: https://arxiv.org/abs/0906.3440
- name: PDF
  url: files/publications/feito-measuring-2009.pdf
---
