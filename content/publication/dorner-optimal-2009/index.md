---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Optimal Quantum Phase Estimation
subtitle: ''
summary: ''
authors:
- U. Dorner
- R. Demkowicz-Dobrzanski
- B. J. Smith
- Jeff S. Lundeen
- W. Wasilewski
- K. Banaszek
- I. A. Walmsley
tags: []
categories: []
date: '2009-01-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.435317Z'
publication_types:
- '2'
abstract: By using a systematic optimization approach, we determine quantum states
  of light with definite photon number leading to the best possible precision in optical
  two-mode interferometry. Our treatment takes into account the experimentally relevant
  situation of photon losses. Our results thus reveal the benchmark for precision
  in optical interferometry. Although this boundary is generally worse than the Heisenberg
  limit, we show that the obtained precision beats the standard quantum limit, thus
  leading to a significant improvement compared to classical interferometers. We furthermore
  discuss alternative states and strategies to the optimized states which are easier
  to generate at the cost of only slightly lower precision.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.102.040403
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.102.040403
- name: arXiv
  url: https://arxiv.org/abs/0807.3659
- name: PDF
  url: files/publications/dorner-optimal-2009.pdf
---
