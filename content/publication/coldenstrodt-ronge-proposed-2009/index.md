---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A proposed testbed for detector tomography
subtitle: ''
summary: ''
authors:
- Hendrik B. Coldenstrodt-Ronge
- Jeff S. Lundeen
- Kenny L. Pregnell
- Alvaro Feito
- Brian J. Smith
- Wolfgang Mauerer
- Christine Silberhorn
- Jens Eisert
- Martin B. Plenio
- Ian A. Walmsley
tags:
- measurement
- photon counting
- POVM
- quantum
- tomography
- Wigner-function
categories: []
date: '2009-01-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.659652Z'
publication_types:
- '2'
abstract: Measurement is the only part of a general quantum system that has yet to
  be characterised experimentally in a complete manner. Detector tomography provides
  a procedure for doing just this; an arbitrary measurement device can be fully characterised,
  and thus calibrated, in a systematic way without access to its components or its
  design. The result is a reconstructed POVM containing the measurement operators
  associated with each measurement outcome. We consider two detectors, a single-photon
  detector and a photon-number counter, and propose an easily realised experimental
  apparatus to perform detector tomography on them. We also present a method of visualising
  the resulting measurement operators.
publication: '*Journal of Modern Optics*'
doi: 10.1080/09500340802304929
links:
- name: URL
  url: https://doi.org/10.1080/09500340802304929
- name: arXiv
  url: https://arxiv.org/abs/0902.4384
- name: PDF
  url: files/publications/coldenstrodt-ronge-proposed-2009.pdf
---
