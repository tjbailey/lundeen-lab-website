---
# Documentation: https://wowchemy.com/docs/managing-content/

title: General lossless spatial polarization transformations
subtitle: ''
summary: ''
authors:
- Alicia Sit
- Lambert Giner
- Ebrahim Karimi
- Jeff S. Lundeen
tags: []
categories: []
date: '2017-08-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.399908Z'
publication_types:
- '2'
abstract: Liquid crystals allow for the real-time control of the polarization of light.
  We describe and provide some experimental examples of the types of general polarization
  transformations, including universal polarization transformations, that can be accomplished
  with liquid crystals in tandem with fixed waveplates. Implementing these transformations
  with an array of liquid crystals, e.g. a spatial light modulator, allows for the
  manipulation of the polarization across a beam’s transverse plane. We outline applications
  of such general spatial polarization transformations in the generation of exotic
  types of vector polarized beams, a polarization magnifier, and the correction of
  polarization aberrations in light fields.
publication: '*Journal of Optics*'
doi: 10.1088/2040-8986/aa7f65
links:
- name: URL
  url: https://doi.org/10.1088/2040-8986/aa7f65
- name: arXiv
  url: https://arxiv.org/abs/1702.06108
- name: PDF
  url: files/publications/sit-general-2017.pdf
---
