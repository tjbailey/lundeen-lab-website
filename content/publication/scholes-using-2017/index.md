---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Using coherence to enhance function in chemical and biophysical systems
subtitle: ''
summary: ''
authors:
- Gregory D. Scholes
- Graham R. Fleming
- Lin X. Chen
- Alán Aspuru-Guzik
- Andreas Buchleitner
- David F. Coker
- Gregory S. Engel
- Rienk van Grondelle
- Akihito Ishizaki
- David M. Jonas
- Jeff S. Lundeen
- James K. McCusker
- Shaul Mukamel
- Jennifer P. Ogilvie
- Alexandra Olaya-Castro
- Mark A. Ratner
- Frank C. Spano
- K. Birgitta Whaley
- Xiaoyang Zhu
tags:
- Chemistry
- Energy harvesting
- Physical chemistry
categories: []
date: '2017-03-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.639969Z'
publication_types:
- '2'
abstract: Coherence phenomena arise from interference, or the addition, of wave-like
  amplitudes with fixed phase differences. Although coherence has been shown to yield
  transformative ways for improving function, advances have been confined to pristine
  matter and coherence was considered fragile. However, recent evidence of coherence
  in chemical and biological systems suggests that the phenomena are robust and can
  survive in the face of disorder and noise. Here we survey the state of recent discoveries,
  present viewpoints that suggest that coherence can be used in complex chemical systems,
  and discuss the role of coherence as a design element in realizing function.
publication: '*Nature*'
doi: 10.1038/nature21425
links:
- name: URL
  url: https://www.nature.com/articles/nature21425
- name: PDF
  url: files/publications/schole-using-2017.pdf
---
