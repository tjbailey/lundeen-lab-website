---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Direct measurement of the quantum wavefunction
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- Brandon Sutherland
- Aabid Patel
- Corey Stewart
- Charles Bamber
tags:
- Quantum mechanics
- Theoretical chemistry
categories: []
date: '2011-06-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.231936Z'
publication_types:
- '2'
abstract: The wavefunction, describing both the wave-like and the particle-like nature
  of everything in the Universe, is central to quantum theory. Physicists usually
  learn about it indirectly in tomographic experiments that measure only some aspects
  of its behaviour. Now a team from Canada's Institute for National Measurement Standards
  has developed a new and gentle technique that makes it possible to observe the wavefunction
  directly. They demonstrate the approach by measuring the transverse spatial wavefunction
  of a single photon. The discovery that the wavefunction can be probed directly provides
  a tool that could prove useful in a wide range of fields, and raises questions bordering
  on the philosophical about what the wavefunction actually is.
publication: '*Nature*'
doi: 10.1038/nature10120
links:
- name: URL
  url: https://www.nature.com/articles/nature10120
- name: arXiv
  url: https://arxiv.org/abs/1112.3575
- name: PDF
  url: files/publications/lundeen-direct-2011.pdf
---
