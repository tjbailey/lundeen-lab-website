---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A characterization of the single-photon sensitivity of an electron multiplying
  charge-coupled device
subtitle: ''
summary: ''
authors:
- Lijian Zhang
- Leonardo Neves
- Jeff S. Lundeen
- Ian A. Walmsley
tags: []
categories: []
date: '2009-05-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.151373Z'
publication_types:
- '2'
abstract: We experimentally characterize the performance of the electron multiplying
  charge-coupled device (EMCCD) camera for the detection of single photons. The tests
  are done with the photon pairs generated from parametric downconversion (PDC). The
  gain, time response and noise performance of the EMCCD are characterized. In addition,
  we attempt to use the camera to measure the spatial correlations of PDC. The results
  reveal the capabilities and limits of the EMCCD as a single-photon-detector array
  for the applications of quantum optics, astronomy and microscopy.
publication: '*Journal of Physics B: Atomic, Molecular and Optical Physics*'
doi: 10.1088/0953-4075/42/11/114011
links:
- name: URL
  url: https://doi.org/10.1088/0953-4075/42/11/114011
- name: PDF
  url: files/publications/zhang-characterization-2009.pdf
---
