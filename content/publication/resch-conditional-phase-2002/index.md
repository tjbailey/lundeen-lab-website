---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Conditional-Phase Switch at the Single-Photon Level
subtitle: ''
summary: ''
authors:
- Kevin J. Resch
- Jeff S. Lundeen
- Aephraim M. Steinberg
tags: []
categories: []
date: '2002-06-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.603575Z'
publication_types:
- '2'
abstract: We present an experimental realization of a two-photon conditional phase
  switch, related to the “ c−φ” gate of quantum computation. This gate relies on quantum
  interference between photon pairs and generates entanglement between two optical
  modes through the process of spontaneous parametric down-conversion (SPDC). The
  interference effect serves to enhance the effective nonlinearity by many orders
  of magnitude, so it is significant at the quantum (single-photon) level. By adjusting
  the relative optical phase between the classical pump for SPDC and the pair of input
  modes, one can impress a large phase shift on one beam which depends on the presence
  or absence of a single photon in a control mode.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.89.037904
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.89.037904
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0205109
- name: PDF
  url: files/publications/resch-conditional-phase-2002.pdf
---
