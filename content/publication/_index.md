---
title: Publications
summary: All refereed publications authored by Jeff Lundeen.

# Listing view
view: citation

# Optional banner image (relative to `assets/media/` folder).
banner:
  caption: ''
  image: ''
---
## Compiled Publication Lists
[Google Scholar](https://scholar.google.com/citations?user=utTnl-QAAAAJ&hl=en)  
[arXiv - preprints of most of my publications](https://arxiv.org/search/?query=Jeff+Lundeen&searchtype=author)

## Refereed Publications