---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Heralded Generation of Ultrafast Single Photons in Pure Quantum States
subtitle: ''
summary: ''
authors:
- Peter J. Mosley
- Jeff S. Lundeen
- Brian J. Smith
- Piotr Wasylczyk
- Alfred B. U’Ren
- Christine Silberhorn
- Ian A. Walmsley
tags: []
categories: []
date: '2008-04-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.241312Z'
publication_types:
- '2'
abstract: We present an experimental demonstration of heralded single photons prepared
  in pure quantum states from a parametric down-conversion source. It is shown that,
  through controlling the modal structure of the photon pair emission, one can generate
  pairs in factorable states and thence eliminate the need for spectral filters in
  multiple-source interference schemes. Indistinguishable heralded photons were generated
  in two independent spectrally engineered sources and Hong-Ou-Mandel interference
  observed between them without spectral filters. The measured visibility of 94.4%
  sets a minimum bound on the mean photon purity.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.100.133601
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.100.133601
- name: arXiv
  url: https://arxiv.org/abs/0711.1054
- name: PDF
  url: files/publications/mosley-heralded-2008.pdf
---
