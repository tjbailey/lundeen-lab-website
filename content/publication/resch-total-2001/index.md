---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Total reflection cannot occur with a negative delay time
subtitle: ''
summary: ''
authors:
- K. J. Resch
- Jeff S. Lundeen
- A. M. Steinberg
tags:
- Delay effects
- Dielectric materials
- Optical interferometry
- Optical materials
- Optical propagation
- Optical reflection
- Pulse amplifiers
- Reflectivity
- Taylor series
- Tunneling
categories: []
date: '2001-06-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.709620Z'
publication_types:
- '2'
abstract: It was recently predicted [ in ibid., vol. 33, p. 519, 1997] that the frustrated
  Gires-Tournois interferometer exhibits a negative delay time for reflection. Given
  its 100% reflectivity, this appears to contradict causality. We demonstrate that
  an additional, positive, contribution comes from consideration of the transverse
  dimension. We prove that this contribution is always large enough to enforce a positive
  total delay.
publication: '*IEEE Journal of Quantum Electronics*'
doi: 10.1109/3.922777
links:
- name: URL
  url: https://ieeexplore.ieee.org/document/922777
- name: PDF
  url: files/publications/resch-total-2001.pdf
---
