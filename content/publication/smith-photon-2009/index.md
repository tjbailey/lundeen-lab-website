---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Photon pair generation in birefringent optical fibers
subtitle: ''
summary: ''
authors:
- Brian J. Smith
- P. Mahou
- Offir Cohen
- Jeff S. Lundeen
- I. A. Walmsley
tags: []
categories: []
date: '2009-12-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.525377Z'
publication_types:
- '2'
abstract: We study both experimentally and theoretically the generation of photon
  pairs by spontaneous four-wave mixing (SFWM) in standard birefringent optical fibers.
  The ability to produce a range of two-photon spectral states, from highly correlated
  (entangled) to completely factorable, by means of cross-polarized birefringent phase
  matching, is explored. A simple model is developed to predict the spectral state
  of the photon pair which shows how this can be adjusted by choosing the appropriate
  pump bandwidth, fiber length and birefringence. Spontaneous Raman scattering is
  modeled to determine the tradeoff between SFWM and background Raman noise, and the
  predicted results are shown to agree with experimental data.
publication: '*Optics Express*'
doi: 10.1364/OE.17.023589
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-17-26-23589
- name: arXiv
  url: https://arxiv.org/abs/0909.4319
- name: PDF
  url: files/publications/smith-photon-2009.pdf
---
