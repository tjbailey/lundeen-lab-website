---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'A short perspective on long crystals: broadband wave mixing and its application
  to ultrafast quantum optics'
subtitle: ''
summary: ''
authors:
- P. Wasylczyk
- A. B. U'ren
- P. J. Mosley
- Jeff Lundeen
- M. P. A. Branderhorst
- S.-P. Gorza
- A. Monmayrant
- A. Radunsky
- I. A. Walmsley
tags: []
categories: []
date: '2007-09-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.650997Z'
publication_types:
- '2'
abstract: We present an overview of recently developed ideas in ultrafast nonlinear
  optics, and describe three applications where these ideas have had an impact. A
  closer look at three wave mixing of broadband electromagnetic fields in birefringent
  nonlinear crystals shows that not only phase matching, but also group velocity matching
  is important for understanding the process of up- and down-conversion with ultrashort
  laser pulses. In fact the higher-order dispersion of nonlinear crystalline materials
  provides an underused degree of freedom that allows tailoring the interaction so
  that it is suitable for a number of different applications. We analyse the processes
  of parametric downconversion for the production of pure single photon states, and
  upconversion for ultrashort pulse characterization and for quantum state and process
  tomography in molecules.
publication: '*Journal of Modern Optics*'
doi: 10.1080/09500340701432599
links:
- name: URL
  url: https://doi.org/10.1080/09500340701432599
- name: PDF
  url: files/publications/wasylczk-short-2007.pdf
---
