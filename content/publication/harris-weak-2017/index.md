---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Weak Value Amplification Can Outperform Conventional Measurement in the Presence
  of Detector Saturation
subtitle: ''
summary: ''
authors:
- Jérémie Harris
- Robert W. Boyd
- Jeff S. Lundeen
tags: []
categories: []
date: '2017-02-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.733487Z'
publication_types:
- '2'
abstract: 'Weak value amplification (WVA) is a technique by which one can magnify
  the apparent strength of a measurement signal. Some have claimed that WVA can outperform
  more conventional measurement schemes in parameter estimation. Nonetheless, a significant
  body of theoretical work has challenged this perspective, suggesting WVA to be fundamentally
  suboptimal. Optimal measurements may not be practical, however. Two practical considerations
  that have been conjectured to afford a benefit to WVA over conventional measurement
  are certain types of noise and detector saturation. Here, we report a theoretical
  study of the role of saturation and pixel noise in WVA-based measurement, in which
  we carry out a Bayesian analysis of the Fisher information available using a saturable,
  pixelated, digitized, and/or noisy detector. We draw two conclusions: first, that
  saturation alone does not confer an advantage to the WVA approach over conventional
  measurement, and second, that WVA can outperform conventional measurement when saturation
  is combined with intrinsic pixel noise and/or digitization.'
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.118.070802
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.118.070802
- name: arXiv
  url: https://arxiv.org/abs/1612.04327
- name: PDF
  url: files/publications/harris-weak-2017.pdf
---
