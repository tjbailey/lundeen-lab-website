---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Measurement of the transverse electric field profile of light by a self-referencing
  method with direct phase determination
subtitle: ''
summary: ''
authors:
- C. Bamber
- B. Sutherland
- A. Patel
- C. Stewart
- Jeff S. Lundeen
tags: []
categories: []
date: '2012-01-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.717770Z'
publication_types:
- '2'
abstract: We present a method for measuring the transverse electric field profile
  of a beam of light which allows for direct phase retrieval. The measured values
  correspond, within a normalization constant, to the real and imaginary parts of
  the electric field in a plane normal to the direction of propagation. This technique
  represents a self-referencing method for probing the wavefront characteristics of
  light.
publication: '*Optics Express*'
doi: 10.1364/OE.20.002034
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-20-3-2034
- name: PDF
  url: files/publications/bamber-measurement-2012.pdf
---
