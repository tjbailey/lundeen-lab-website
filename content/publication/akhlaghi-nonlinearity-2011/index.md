---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Nonlinearity in single photon detection: modeling and quantum tomography'
subtitle: ''
summary: ''
authors:
- Mohsen K. Akhlaghi
- A. Hamed Majedi
- Jeff S. Lundeen
tags: []
categories: []
date: '2011-10-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.141405Z'
publication_types:
- '2'
abstract: Single Photon Detectors are integral to quantum optics and quantum information.
  Superconducting Nanowire based detectors exhibit new levels of performance, but
  have no accepted quantum optical model that is valid for multiple input photons.
  By performing Detector Tomography, we improve the recently proposed model [M.K.
  Akhlaghi and A.H. Majedi, IEEE Trans. Appl. Supercond. 19, 361 (2009)] and also
  investigate the manner in which these detectors respond nonlinearly to light, a
  valuable feature for some applications. We develop a device independent model for
  Single Photon Detectors that incorporates this nonlinearity.
publication: '*Optics Express*'
doi: 10.1364/OE.19.021305
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-19-22-21305
- name: arXiv
  url: https://arxiv.org/abs/1108.3815
- name: PDF
  url: files/publications/akhlaghi-nonlinearity-2011.pdf
---
