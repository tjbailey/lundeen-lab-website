---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Weak-value measurements can outperform conventional measurements
subtitle: ''
summary: ''
authors:
- Omar S. Magaña-Loaiza
- Jérémie Harris
- Jeff S. Lundeen
- Robert W. Boyd
tags: []
categories: []
date: '2016-12-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.849952Z'
publication_types:
- '2'
abstract: In this paper we provide a simple, straightforward example of a specific
  situation in which weak-value amplification (WVA) clearly outperforms conventional
  measurement in determining the angular orientation of an optical component. We also
  offer a perspective reconciling the views of some theorists, who claim WVA to be
  inherently sub-optimal for parameter estimation, with the perspective of the many
  experimentalists and theorists who have used the procedure to successfully access
  otherwise elusive phenomena.
publication: '*Physica Scripta*'
doi: 10.1088/1402-4896/92/2/023001
links:
- name: URL
  url: https://doi.org/10.1088/1402-4896/92/2/023001
- name: PDF
  url: files/publications/magana-loaiza-weak-values-2016.pdf
---
