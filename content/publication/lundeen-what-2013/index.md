---
# Documentation: https://wowchemy.com/docs/managing-content/

title: What Can we Say about a Photon’s Past?
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
tags: []
categories: []
date: '2013-12-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.398881Z'
publication_types:
- '2'
abstract: An experiment demonstrates that even when physicists think a quantum particle
  has followed a single path it might not have.
publication: '*Physics*'
doi: 10.1103/PhysRevLett.111.240402
links:
- name: URL
  url: https://physics.aps.org/articles/v6/133?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Science360NewsServiceComplete+(Science360+News+Service%3A+Complete)
- name: PDF
  url: files/publications/lundeen-what-2013.pdf
---
