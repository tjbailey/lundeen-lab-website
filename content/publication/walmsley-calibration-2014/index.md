---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Calibration of particle detectors
subtitle: ''
summary: ''
authors:
- Ian Alexander Walmsley
- Brian John Smith
- Jeff S. Lundeen
- Peter James Mosley
- Graciana Puentes
- Hendrik Bernd Coldenstrodt-Ronge
- Nicholas Lloyd Thomas-Peter
- Andrew Philip Worsley
tags:
- detection efficiency
- detector arrangement
- optical detector
- path
- photon
categories: []
date: '2014-04-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.046813Z'
publication_types:
- '8'
abstract: ''
publication: ''
links:
- name: URL
  url: https://patents.google.com/patent/US8706437B2/en
---
