---
# Documentation: https://wowchemy.com/docs/managing-content/

title: To what extent can space be compressed? Bandwidth limits of spaceplates
subtitle: ''
summary: ''
authors:
- Kunal Shastri
- Orad Reshef
- Robert W. Boyd
- Jeff S. Lundeen
- Francesco Monticone
tags:
- Diffractive lenses; Guided mode resonance; Light propagation; Optical components;
  Optical systems; Refractive index
categories: []
date: '2022-07-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-17T21:35:35.795774Z'
publication_types:
- '2'
abstract: Spaceplates are novel flat-optic devices that implement the optical response
  of a free-space volume over a smaller length, effectively ``compressing space''
  for light propagation. Together with flat lenses such as metalenses or diffractive
  lenses, spaceplates have the potential to enable the miniaturization of any free-space
  optical system. While the fundamental and practical bounds on the performance metrics
  of flat lenses have been well studied in recent years, a similar understanding of
  the ultimate limits of spaceplates is lacking, especially regarding the issue of
  bandwidth, which remains as a crucial roadblock for the adoption of this platform.
  In this work, we derive fundamental bounds on the bandwidth of spaceplates as a
  function of their numerical aperture and compression ratio (ratio by which the free-space
  pathway is compressed). The general form of these bounds is universal and can be
  applied and specialized for different broad classes of space-compression devices,
  regardless of their particular implementation. Our findings also offer relevant
  insights into the physical mechanism at the origin of generic space-compression
  effects and may guide the design of higher performance spaceplates, opening new
  opportunities for ultra-compact, monolithic, planar optical systems for a variety
  of applications.
publication: '*Optica*'
doi: 10.1364/OPTICA.455680
links:
- name: URL
  url: https://opg.optica.org/optica/abstract.cfm?URI=optica-9-7-738
- name: arXiv
  url: https://arxiv.org/abs/2201.11340
- name: PDF
  url: files/publications/shastri-what-2022.pdf
---
