---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Tailored Photon-Pair Generation in Optical Fibers
subtitle: ''
summary: ''
authors:
- Offir Cohen
- Jeff S. Lundeen
- Brian J. Smith
- Graciana Puentes
- Peter J. Mosley
- Ian A. Walmsley
tags: []
categories: []
date: '2009-03-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.240355Z'
publication_types:
- '2'
abstract: We experimentally control the spectral structure of photon pairs created
  via spontaneous four-wave mixing in microstructured fibers. By fabricating fibers
  with designed dispersion, one can manipulate the photons’ wavelengths, joint spectrum,
  and, thus, entanglement. As an example, we produce photon pairs with no spectral
  correlations, allowing direct heralding of single photons in pure-state wave packets
  without filtering. We achieve an experimental purity of (85.9±1.6)%, while theoretical
  analysis and preliminary tests suggest that 94.5% purity is possible with a much
  longer fiber.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.102.123603
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.102.123603
- name: arXiv
  url: https://arxiv.org/abs/0809.0071
- name: PDF
  url: files/publications/cohen-tailored-2009.pdf
---
