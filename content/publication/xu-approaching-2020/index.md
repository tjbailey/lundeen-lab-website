---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Approaching Quantum-Limited Metrology with Imperfect Detectors by Using Weak-Value
  Amplification
subtitle: ''
summary: ''
authors:
- Liang Xu
- Zexuan Liu
- Animesh Datta
- George C. Knee
- Jeff S. Lundeen
- Yan-qing Lu
- Lijian Zhang
tags: []
categories: []
date: '2020-08-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.209625Z'
publication_types:
- '2'
abstract: Weak-value amplification (WVA) is a metrological protocol that amplifies
  ultrasmall physical effects. However, the amplified outcomes necessarily occur with
  highly suppressed probabilities, leading to the extensive debate on whether the
  overall measurement precision is improved in comparison to that of conventional
  measurement (CM). Here, we experimentally demonstrate the unambiguous advantages
  of WVA that overcome practical limitations including noise and saturation of photodetection
  and maintain a shot-noise-scaling precision for a large range of input light intensity
  well beyond the dynamic range of the photodetector. The precision achieved by WVA
  is 6 times higher than that of CM in our setup. Our results clear the way for the
  widespread use of WVA in applications involving the measurement of small signals
  including precision metrology and commercial sensors.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.125.080501
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.125.080501
- name: arXiv
  url: https://arxiv.org/abs/2005.03629
- name: PDF
  url: files/publications/xu-approaching-2020.pdf
---
