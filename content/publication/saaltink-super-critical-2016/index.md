---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Super-critical phasematching for photon pair generation in structured light
  modes
subtitle: ''
summary: ''
authors:
- Rebecca Y. Saaltink
- Lambert Giner
- Robert W. Boyd
- Ebrahim Karimi
- Jeff S. Lundeen
tags: []
categories: []
date: '2016-10-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.021536Z'
publication_types:
- '2'
abstract: We propose a method for directly producing radially and azimuthally polarized
  photon pairs through spontaneous parametric downconversion (SPDC). This method constitutes
  a novel geometry for SPDC, in which a radially polarized Bessel-Gauss pump beam
  is directed into a nonlinear crystal, with the central propagation direction parallel
  to the crystal axis. The phasematching conditions are controlled by changing the
  opening angle of the pump beam; as the crystal axis cannot be tuned, we refer to
  this process as super-critical phasematching. We model and plot the spatial and
  polarization output distributions for Type-I and Type-II super-critical phasematching.
publication: '*Optics Express*'
doi: 10.1364/OE.24.024495
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-24-21-24495
- name: arXiv
  url: https://arxiv.org/abs/1610.01454
- name: PDF
  url: files/publications/saaltink-super-critical-2016.pdf
---
