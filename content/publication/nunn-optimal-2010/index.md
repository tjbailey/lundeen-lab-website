---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Optimal experiment design for quantum state tomography: Fair, precise, and
  minimal tomography'
subtitle: ''
summary: ''
authors:
- J. Nunn
- B. J. Smith
- G. Puentes
- I. A. Walmsley
- Jeff S. Lundeen
tags: []
categories: []
date: '2010-04-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.345977Z'
publication_types:
- '2'
abstract: Given an experimental setup and a fixed number of measurements, how should
  one take data to optimally reconstruct the state of a quantum system? The problem
  of optimal experiment design (OED) for quantum state tomography was first broached
  by Kosut et al. [R. Kosut, I. Walmsley, and H. Rabitz, e-print arXiv:quant-ph/0411093
  (2004)]. Here we provide efficient numerical algorithms for finding the optimal
  design, and analytic results for the case of ‘minimal tomography’. We also introduce
  the average OED, which is independent of the state to be reconstructed, and the
  optimal design for tomography (ODT), which minimizes tomographic bias. Monte Carlo
  simulations confirm the utility of our results for qubits. Finally, we adapt our
  approach to deal with constrained techniques such as maximum-likelihood estimation.
  We find that these are less amenable to optimization than cruder reconstruction
  methods, such as linear inversion.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.81.042109
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.81.042109
- name: arXiv
  url: https://arxiv.org/abs/0911.4310
- name: PDF
  url: files/publications/nunn-optimal-2010.pdf
---
