---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A variable partially polarizing beam splitter
subtitle: ''
summary: ''
authors:
- Jefferson Flórez
- Nathan J. Carlson
- Codey H. Nacke
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-02-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.300629Z'
publication_types:
- '2'
abstract: ''
publication: '*Review of Scientific Instruments*'
doi: 10.1063/1.5004805
links:
- name: URL
  url: https://aip.scitation.org/doi/abs/10.1063/1.5004805
- name: arXiv
  url: https://arxiv.org/abs/1709.05209
- name: PDF
  url: files/publications/florez-variable-2018.pdf

---
