---
# Documentation: https://wowchemy.com/docs/managing-content/

title: The Effect of Non-Gaussian Noise on Auto-correlative Weak-value Amplification
subtitle: ''
summary: ''
authors:
- Jing-Hui Huang
- Jeff S. Lundeen
- Adetunmise C. Dada
- Kyle M. Jordan
- Guang-Jun Wang
- Xue-Ying Duan
- Xiang-Yun Hu
tags:
- Quantum Physics (quant-ph)
- Optics (physics.optics)
- 'FOS: Physical sciences'
- 'FOS: Physical sciences'
categories: []
date: '2022-01-01'
lastmod: 2022-10-17T17:42:24-04:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-17T21:46:33.942008Z'
publication_types:
- '0'
abstract: 'Accurate knowledge of the spectral features of noise and their influence on open quantum systems is fundamental for quantitative understanding and prediction of the dynamics in a realistic environment. For the weak measurements of two-level systems, the weak value obtained from experiments will inevitably be affected by the noise of the environment. Following our earlier work on the technique of the auto-correlative weak-value amplification (AWVA) approach under a Gaussian noise environment, here we study the effect of non-Gaussian noise on the AWVA this http URL particular, two types of noise with a negative-dB signal-to-noise ratio, frequency-stationary noises and frequency-nonstationary noises are studied. The various frequency-stationary noises, including low-frequency (1/f) noises, medium-frequency noises, and high-frequency noises, are generated in Simulink by translating the Gaussian white noise with different band-pass filters. While impulsive noise is studied as an example of frequency-non stationary noises. Our simulated results demonstrate that 1/f noises and impulsive noises have greater disturbance on the AWVA measurements. In addition, adding one kind of frequency-stationary noise, clamping the detected signals, and dominating the measurement range may {have} the potential to improve the precision of the AWVA technique with both a smaller deviation of the mean value and a smaller error bar in the presence of many hostile non-Gaussian noises. '
publication: '*arXiv*'
doi: 10.48550/ARXIV.2209.12732
links:
- name: arXiv
  url: https://arxiv.org/abs/2209.12732
- name: PDF
  url: files/publications/huang-effect-2022.pdf
---
