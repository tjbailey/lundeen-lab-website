---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Group III-V semiconductors as promising nonlinear integrated photonic platforms
subtitle: ''
summary: ''
authors:
- Kaustubh Vyas
- Daniel H. G. Espinosa
- Daniel Hutama
- Shubhendra Kumar Jain
- Rania Mahjoub
- Ehsan Mobini
- Kashif M. Awan
- Jeff S. Lundeen
- Ksenia Dolgaleva
tags: []
categories: []
date: '2022-01-01'
lastmod: 2022-10-12T18:59:59-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:03.152868Z'
publication_types:
- '2'
abstract: Group III–V semiconductors are based on the elements of groups III and V
  of the periodic table. The possibility to grow thin-films made of binary, ternary,
  and quaternary III–V alloys with different fractions of their constituent elements
  allows for the precise engineering of their optical properties. In addition, since
  many III–V compounds are direct-bandgap semiconductors, they are suitable for the
  development of photonic devices and integrated circuits, especially when monolithic
  integration is required. Moreover, the strong optical nonlinearities of III–V materials
  enable a fertile field of research in photonic devices for all-optical signal processing,
  wavelength conversion, and frequency generation. Experimentally accessing the plethora
  of nonlinear optical phenomena in these materials considerably facilitates the exploration
  of light-matter interactions. Several demonstrations have explored the optical nonlinearities
  in waveguides, microring resonators, photonic crystal structures, quantum dots,
  and lasers. In this review, we survey numerous nonlinear optical studies performed
  in III–V semiconductor waveguide platforms. In particular, we discuss linear and
  nonlinear optical properties, material growth and fabrication processes, newer hybrid
  material platforms, and several nonlinear optical applications of III–V semiconductor
  integrated optical platforms.
publication: '*Advances in Physics: X*'
doi: 10.1080/23746149.2022.2097020
links:
- name: URL
  url: https://doi.org/10.1080/23746149.2022.2097020
- name: PDF
  url: files/publications/doi-10-1080-23746149-2022-2097020.pdf
---
