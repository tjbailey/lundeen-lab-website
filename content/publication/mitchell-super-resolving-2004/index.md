---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Super-resolving phase measurements with a multiphoton entangled state
subtitle: ''
summary: ''
authors:
- M. W. Mitchell
- Jeff S. Lundeen
- A. M. Steinberg
tags:
- Humanities and Social Sciences
- multidisciplinary
- Science
categories: []
date: '2004-05-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:10.364838Z'
publication_types:
- '2'
abstract: Interference phenomena are ubiquitous in physics, often forming the basis
  of demanding measurements. Examples include Ramsey interferometry in atomic spectroscopy,
  X-ray diffraction in crystallography and optical interferometry in gravitational-wave
  studies1,2. It has been known for some time that the quantum property of entanglement
  can be exploited to perform super-sensitive measurements, for example in optical
  interferometry or atomic spectroscopy3,4,5,6,7. The idea has been demonstrated for
  an entangled state of two photons8, but for larger numbers of particles it is difficult
  to create the necessary multiparticle entangled states9,10,11. Here we demonstrate
  experimentally a technique for producing a maximally entangled three-photon state
  from initially non-entangled photons. The method can in principle be applied to
  generate states of arbitrary photon number, giving arbitrarily large improvement
  in measurement resolution12,13,14,15. The method of state construction requires
  non-unitary operations, which we perform using post-selected linear-optics techniques
  similar to those used for linear-optics quantum computing16,17,18,19,20.
publication: '*Nature*'
doi: 10.1038/nature02493
links:
- name: URL
  url: https://www.nature.com/articles/nature02493
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0306198
- name: PDF
  url: files/publications/mitchel-super-resolving-2004.pdf
---
