---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Conditional preparation of single photons using parametric downconversion:
  a recipe for purity'
subtitle: ''
summary: ''
authors:
- P. J. Mosley
- Jeff S. Lundeen
- B. J. Smith
- I. A. Walmsley
tags: []
categories: []
date: '2008-09-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.048744Z'
publication_types:
- '2'
abstract: In an experiment reported recently (Mosley et al 2008 Phys. Rev. Lett. 100
  133601), we demonstrated that, through group velocity matched parametric downconversion,
  heralded single photons can be generated in pure quantum states without spectral
  filtering. The technique relies on factorable photon pair production, initially
  developed theoretically in the strict collinear regime; focusing—required in any
  experimental implementation—can ruin this factorability. Here, we present the numerical
  model used to design our single photon sources and minimize spectral correlations
  in the light of such experimental considerations. Furthermore, we show that the
  results of our model are in good agreement with measurements made on the photon
  pairs and give a detailed description of the exact requirements for constructing
  this type of source.
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/10/9/093011
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/10/9/093011
- name: arXiv
  url: https://arxiv.org/abs/0807.1409
- name: PDF
  url: files/publications/mosley-conditional-2008.pdf
---
