---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Mapping coherence in measurement via full quantum tomography of a hybrid optical
  detector
subtitle: ''
summary: ''
authors:
- Lijian Zhang
- Hendrik B. Coldenstrodt-Ronge
- Animesh Datta
- Graciana Puentes
- Jeff S. Lundeen
- Xian-Min Jin
- Brian J. Smith
- Martin B. Plenio
- Ian A. Walmsley
tags:
- Optical sensors
- Quantum optics
categories: []
date: '2012-06-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.527521Z'
publication_types:
- '2'
abstract: Quantum states and measurements exhibit wave-like (continuous) or particle-like
  (discrete) character. Hybrid discrete–continuous photonic systems are key to investigating
  fundamental quantum phenomena1,2,3, generating superpositions of macroscopic states4,
  and form essential resources for quantum-enhanced applications5 such as entanglement
  distillation6,7 and quantum computation8, as well as highly efficient optical telecommunications9,10.
  Realizing the full potential of these hybrid systems requires quantum-optical measurements
  sensitive to non-commuting observables such as field quadrature amplitude and photon
  number11,12,13. However, a thorough understanding of the practical performance of
  an optical detector interpolating between these two regions is absent. Here, we
  report the implementation of full quantum detector tomography, enabling the characterization
  of the simultaneous wave and photon-number sensitivities of quantum-optical detectors.
  This yields the largest parameterization to date in quantum tomography experiments,
  requiring the development of novel theoretical tools. Our results reveal the role
  of coherence in quantum measurements and demonstrate the tunability of hybrid quantum-optical
  detectors.
publication: '*Nature Photonics*'
doi: 10.1038/nphoton.2012.107
links:
- name: URL
  url: https://www.nature.com/articles/nphoton.2012.107
- name: arXiv
  url: https://arxiv.org/abs/1204.1893
- name: PDF
  url: files/publications/zhang-mapping-2012.pdf
---
