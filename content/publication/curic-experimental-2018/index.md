---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental investigation of measurement-induced disturbance and time symmetry
  in quantum physics
subtitle: ''
summary: ''
authors:
- Davor Curic
- M. C. Richardson
- G. S. Thekkadath
- Jefferson Flórez
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-04-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.210450Z'
publication_types:
- '2'
abstract: 'Unlike regular time evolution governed by the Schrödinger equation, standard
  quantum measurement appears to violate time-reversal symmetry. Measurement creates
  random disturbances (e.g., collapse) that prevent back-tracing the quantum state
  of the system. The effect of these disturbances is explicit in the results of subsequent
  measurements. In this way, the joint result of sequences of measurements depends
  on the order in time in which those measurements are performed. One might expect
  that if the disturbance could be eliminated this time-ordering dependence would
  vanish. Following a recent theoretical proposal [Bednorz, Franke, and Belzig, New
  J. Phys. 15, 023043 (2013)], we experimentally investigate this dependence for a
  kind of measurement that creates an arbitrarily small disturbance: weak measurement.
  We perform various sequences of a set of polarization weak measurements on photons.
  We experimentally demonstrate that, although the weak measurements are minimally
  disturbing, their time ordering affects the outcome of the measurement sequence
  for quantum systems.'
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.97.042128
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.97.042128
- name: arXiv
  url: https://arxiv.org/abs/1801.04362
- name: PDF
  url: files/publications/curic-experimental-2018.pdf
---
