---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Photon-exchange effects on photon-pair transmission
subtitle: ''
summary: ''
authors:
- K. J. Resch
- G. G. Lapaire
- Jeff S. Lundeen
- J. E. Sipe
- A. M. Steinberg
tags: []
categories: []
date: '2004-06-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:10.673550Z'
publication_types:
- '2'
abstract: It has been proposed that photon-exchange effects associated with virtual
  atomic absorption could have widespread application in quantum information processing.
  Here we investigate simpler exchange effects associated with real absorption as
  modeled by an equivalent linear optical filter. Using nonclassical pairs of photons
  with variable time separation, we observe a maximum suppression of pair transmission
  by at least 5% with respect to the result for independent photons.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.69.063814
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.69.063814
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0306198
- name: PDF
  url: files/publications/resch-photon-exchange-2004.pdf
---
