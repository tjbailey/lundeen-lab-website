---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Pump depletion in parametric down-conversion with low pump energies
subtitle: ''
summary: ''
authors:
- Jefferson Flórez
- Jeff S. Lundeen
- Maria V. Chekhova
tags: []
categories: []
date: '2020-08-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-17T22:10:59.055578Z'
publication_types:
- '2'
abstract: We report the efficient generation of high-gain parametric down-conversion,
  including pump depletion, with pump powers as low as 100 µW (energies 0.1 µJ/pulse)
  and conversion efficiencies up to 33%. In our simple configuration, the pump beam
  is tightly focused into a bulk periodically poled lithium niobate crystal placed
  in free space. We also observe a change in the photon number statistics for both
  the pump and down-converted beams as the pump power increases to reach the depleted
  pump regime. The experimental results are a clear signature of the interplay between
  the pump and the down-converted beams in highly efficient parametric down-conversion
  sources.
publication: '*Optics Letters*'
doi: 10.1364/OL.394925
links:
- name: URL
  url: https://opg.optica.org/ol/abstract.cfm?uri=ol-45-15-4264
- name: arXiv
  url: https://arxiv.org/abs/2003.07483
- name: PDF
  url: files/publications/florez-pump-2020.pdf
---
