---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental Application of Decoherence-Free Subspaces in an Optical Quantum-Computing
  Algorithm
subtitle: ''
summary: ''
authors:
- M. Mohseni
- Jeff S. Lundeen
- K. J. Resch
- A. M. Steinberg
tags: []
categories: []
date: '2003-10-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:10.763212Z'
publication_types:
- '2'
abstract: For a practical quantum computer to operate, it is essential to properly
  manage decoherence. One important technique for doing this is the use of “decoherence-free
  subspaces” (DFSs), which have recently been demonstrated. Here we present the first
  use of DFSs to improve the performance of a quantum algorithm. An optical implementation
  of the Deutsch-Jozsa algorithm can be made insensitive to a particular class of
  phase noise by encoding information in the appropriate subspaces; we observe a reduction
  of the error rate from 35% to 7%, essentially its value in the absence of noise.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.91.187903
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.91.187903
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0212134
- name: PDF
  url: files/publications/mohseni-experimental-2003.pdf
---
