---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Nonlinear Optics with Less Than One Photon
subtitle: ''
summary: ''
authors:
- K. J. Resch
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2001-08-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.237248Z'
publication_types:
- '2'
abstract: We demonstrate suppression and enhancement of spontaneous parametric down-conversion
  via quantum interference with two weak fields from a local oscillator (LO). Effectively,
  pairs of LO photons up-convert with high efficiency for appropriate phase settings,
  exhibiting an effective nonlinearity enhanced by at least 10 orders of magnitude.
  This constitutes a two-photon switch and promises to be applicable to a wide variety
  of quantum nonlinear optical phenomena.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.87.123603
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.87.123603
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0101020
- name: PDF
  url: files/publications/resch-nonlinear-2001.pdf
---
