---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Classical dispersion-cancellation interferometry
subtitle: ''
summary: ''
authors:
- K. J. Resch
- P. Puvanathasan
- Jeff S. Lundeen
- M. W. Mitchell
- K. Bizheva
tags: []
categories: []
date: '2007-07-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.832746Z'
publication_types:
- '2'
abstract: Even-order dispersion cancellation, an effect previously identified with
  frequency-entangled photons, is demonstrated experimentally for the first time with
  a linear, classical interferometer. A combination of a broad bandwidth laser and
  a high resolution spectrometer was used to measure the intensity correlations between
  anti-correlated optical frequencies. Only 14% broadening of the correlation signal
  is observed when significant material dispersion, enough to broaden the regular
  interferogram by 4250%, is introduced into one arm of the interferometer.
publication: '*Optics Express*'
doi: 10.1364/OE.15.008797
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-15-14-8797
- name: arXiv
  url: https://arxiv.org/abs/0704.3938
- name: PDF
  url: files/publications/resch-classical-2007.pdf
---
