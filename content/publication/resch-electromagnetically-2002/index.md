---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Electromagnetically induced opacity for photon pairs
subtitle: ''
summary: ''
authors:
- K. J. RESCH
- Jeff S. LUNDEEN
- A. M. STEINBERG
tags: []
categories: []
date: '2002-03-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.427503Z'
publication_types:
- '2'
abstract: It is shown that quantum interference with classical beams may be used to
  suppress or enhance the rate of spontaneous photon-pair production from a nonlinear
  crystal. Sum-frequency generation of the classical beams is simultaneously enhanced
  or suppressed via interference with a classical pump. In the extreme case, a crystal
  which is transparent to individual photons may block all photon pairs, converting
  them to 2w. This constitutes a coherent nonlinear response at the single-photon
  level, enhanced by a factor of approximately 10 10 Experimental data and a theoretical
  description are presented, and an attempt is made to delineate the classical and
  quantum aspects of these effects.
publication: '*Journal of Modern Optics*'
doi: 10.1080/09500340110088623
links:
- name: URL
  url: https://doi.org/10.1080/09500340110088623
- name: PDF
  url: files/publications/resch-electromagnetically-2002.pdf
---
