---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental Joint Weak Measurement on a Photon Pair as a Probe of Hardy's
  Paradox
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2009-01-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.752157Z'
publication_types:
- '2'
abstract: It has been proposed that the ability to perform joint weak measurements
  on postselected systems would allow us to study quantum paradoxes. These measurements
  can investigate the history of those particles that contribute to the paradoxical
  outcome. Here we experimentally perform weak measurements of joint (i.e., nonlocal)
  observables. In an implementation of Hardy’s paradox, we weakly measure the locations
  of two photons, the subject of the conflicting statements behind the paradox. Remarkably,
  the resulting weak probabilities verify all of these statements but, at the same
  time, resolve the paradox.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.102.020404
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.102.020404
- name: arXiv
  url: https://arxiv.org/abs/0810.4229
- name: PDF
  url: files/publications/lundeen-experimental-2009.pdf
---
