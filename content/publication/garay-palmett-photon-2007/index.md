---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Photon pair-state preparation with tailored spectral properties by spontaneous
  four-wave mixing in photonic-crystal fiber
subtitle: ''
summary: ''
authors:
- K. Garay-Palmett
- H. J. McGuinness
- Offir Cohen
- Jeff S. Lundeen
- R. Rangel-Rojo
- A. B. U’Ren
- M. G. Raymer
- C. J. McKinstrie
- S. Radic
- I. A. Walmsley
tags: []
categories: []
date: '2007-10-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.552703Z'
publication_types:
- '2'
abstract: We study theoretically the generation of photon pairs by spontaneous four-wave
  mixing (SFWM) in photonic crystal optical fiber. We show that it is possible to
  engineer two-photon states with specific spectral correlation (“entanglement”) properties
  suitable for quantum information processing applications. We focus on the case exhibiting
  no spectral correlations in the two-photon component of the state, which we call
  factorability, and which allows heralding of single-photon pure-state wave packets
  without the need for spectral post filtering. We show that spontaneous four wave
  mixing exhibits a remarkable flexibility, permitting a wider class of two-photon
  states, including ultra-broadband, highly-anticorrelated states.
publication: '*Optics Express*'
doi: 10.1364/OE.15.014870
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-15-22-14870
- name: arXiv
  url: https://arxiv.org/abs/0709.3129
- name: PDF
  url: files/publications/garay-palmett-photon-2007.pdf
---
