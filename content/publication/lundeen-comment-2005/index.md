---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Comment on ``Linear optics implementation of weak values in Hardy's paradox''
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- K. J. Resch
- A. M. Steinberg
tags: []
categories: []
date: '2005-07-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.934653Z'
publication_types:
- '2'
abstract: A recent experimental proposal [S.E. Ahnert and M.C. Payne, Phys. Rev. A
  70, 042102 (2004)] outlines a method to measure the weak value predictions of Aharonov
  in Hardy’s paradox. This proposal contains flaws in the state preparation method
  and the procedure for carrying out the requisite weak measurements. We identify
  previously published solutions to some of the flaws.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.72.016101
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.72.016101
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0504122
- name: PDF
  url: files/publications/lundeen-comment-2005.pdf
---
