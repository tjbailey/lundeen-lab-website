---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Determining Complementary Properties with Quantum Clones
subtitle: ''
summary: ''
authors:
- G. S. Thekkadath
- Rebecca Y. Saaltink
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2017-08-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.541222Z'
publication_types:
- '2'
abstract: In a classical world, simultaneous measurements of complementary properties
  (e.g., position and momentum) give a system’s state. In quantum mechanics, measurement-induced
  disturbance is largest for complementary properties and, hence, limits the precision
  with which such properties can be determined simultaneously. It is tempting to try
  to sidestep this disturbance by copying the system and measuring each complementary
  property on a separate copy. However, perfect copying is physically impossible in
  quantum mechanics. Here, we investigate using the closest quantum analog to this
  copying strategy, optimal cloning. The coherent portion of the generated clones’
  state corresponds to “twins” of the input system. Like perfect copies, both twins
  faithfully reproduce the properties of the input system. Unlike perfect copies,
  the twins are entangled. As such, a measurement on both twins is equivalent to a
  simultaneous measurement on the input system. For complementary observables, this
  joint measurement gives the system’s state, just as in the classical case. We demonstrate
  this experimentally using polarized single photons.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.119.050405
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.119.050405
- name: arXiv
  url: https://arxiv.org/abs/1701.04095
- name: PDF
  url: files/publications/thekkadath-determining-2017.pdf
---
