---
# Documentation: https://wowchemy.com/docs/managing-content/

title: The phase sensitivity of a fully quantum three-mode nonlinear interferometer
subtitle: ''
summary: ''
authors:
- Jefferson Flórez
- Enno Giese
- Davor Curic
- Lambert Giner
- Robert W. Boyd
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-12-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.807175Z'
publication_types:
- '2'
abstract: We study a nonlinear interferometer consisting of two consecutive parametric
  amplifiers, where all three optical fields (pump, signal and idler) are treated
  quantum mechanically, allowing for pump depletion and other quantum phenomena. The
  interaction of all three fields in the final amplifier leads to an interference
  pattern from which we extract the phase uncertainty. We find that the phase uncertainty
  oscillates around a saturation level that decreases as the mean number N of input
  pump photons increases. For optimal interaction strengths, we also find a phase
  uncertainty below the shot-noise level and obtain a Heisenberg scaling . This is
  in contrast to the conventional treatment within the parametric approximation, where
  the Heisenberg scaling is observed as a function of the number of down-converted
  photons inside the interferometer.
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/aaf3d2
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/aaf3d2
- name: arXiv
  url: https://arxiv.org/abs/1808.06136
- name: PDF
  url: files/publications/florez-phase-2018.pdf
---
