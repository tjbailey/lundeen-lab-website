---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Theory of four-wave mixing of cylindrical vector beams in optical fibers
subtitle: ''
summary: ''
authors:
- E. Scott Goudreau
- Connor Kupchak
- Benjamin J. Sussman
- Robert W. Boyd
- Jeff S. Lundeen
tags: []
categories: []
date: '2020-06-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.421826Z'
publication_types:
- '2'
abstract: Cylindrical vector (CV) beams are a set of transverse spatial modes that
  exhibit a cylindrically symmetric intensity profile and a variable polarization
  about the beam axis. They are composed of a non-separable superposition of orbital
  and spin angular momenta. Critically, CV beams are also the eigenmodes of optical
  fiber and, as such, are of widespread practical importance in photonics and have
  the potential to increase communications bandwidth through spatial multiplexing.
  Here, we derive the coupled amplitude equations that describe the four-wave mixing
  (FWM) of CV beams in optical fibers. These equations allow us to determine the selection
  rules that govern the interconversion of CV modes in FWM processes. With these selection
  rules, we show that FWM conserves the total angular momentum, the sum of orbital
  and spin angular momenta, in the conversion of two input photons to two output photons.
  When applied to spontaneous FWM, the selection rules show that photon pairs can
  be generated in CV modes directly and can be entangled in those modes. Such quantum
  states of light in CV modes could benefit technologies such as quantum key distribution
  with satellites.
publication: '*JOSA B*'
doi: 10.1364/JOSAB.386622
links:
- name: URL
  url: https://opg.optica.org/josab/abstract.cfm?uri=josab-37-6-1670
- name: arXiv
  url: https://arxiv.org/abs/1912.10109
- name: PDF
  url: files/publications/goudreau-theory-2020.pdf
---
