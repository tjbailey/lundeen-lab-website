---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Quantum phase estimation with lossy interferometers
subtitle: ''
summary: ''
authors:
- R. Demkowicz-Dobrzanski
- U. Dorner
- B. J. Smith
- Jeff S. Lundeen
- W. Wasilewski
- K. Banaszek
- I. A. Walmsley
tags: []
categories: []
date: '2009-07-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:07.898145Z'
publication_types:
- '2'
abstract: 'We give a detailed discussion of optimal quantum states for optical two-mode
  interferometry in the presence of photon losses. We derive analytical formulae for
  the precision of phase estimation obtainable using quantum states of light with
  a definite photon number and prove that maximization of the precision is a convex
  optimization problem. The corresponding optimal precision, i.e., the lowest possible
  uncertainty, is shown to beat the standard quantum limit thus outperforming classical
  interferometry. Furthermore, we discuss more general inputs: states with indefinite
  photon number and states with photons distributed between distinguishable time bins.
  We prove that neither of these is helpful in improving phase estimation precision.'
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.80.013825
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.80.013825
- name: arXiv
  url: https://arxiv.org/abs/0904.0456
- name: PDF
  url: files/publications/demkowicz-dobrazanski-quantum-2009.pdf
---
