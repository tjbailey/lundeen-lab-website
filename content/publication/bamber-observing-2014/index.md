---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Observing Dirac's Classical Phase Space Analog to the Quantum State
subtitle: ''
summary: ''
authors:
- Charles Bamber
- Jeff S. Lundeen
tags: []
categories: []
date: '2014-02-01'
lastmod: 2022-10-12T17:08:12-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.301487Z'
publication_types:
- '2'
abstract: In 1945, Dirac attempted to develop a “formal probability” distribution
  to describe quantum operators in terms of two noncommuting variables, such as position
  x and momentum p [Rev. Mod. Phys. 17, 195 (1945)]. The resulting quasiprobability
  distribution is a complete representation of the quantum state and can be observed
  directly in experiments. We measure Dirac’s distribution for the quantum state of
  the transverse degree of freedom of a photon by weakly measuring transverse x so
  as to not randomize the subsequent p measurement. Furthermore, we show that the
  distribution has the classical-like feature that it transforms (e.g., propagates)
  according to Bayes’ law.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.112.070405
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.112.070405
- name: arXiv
  url: https://arxiv.org/abs/1309.1491
- name: PDF
  url: files/publications/bamber-observing-2014.pdf
---
