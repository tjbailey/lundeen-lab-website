---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Procedure for Direct Measurement of General Quantum States Using Weak Measurement
subtitle: ''
summary: ''
authors:
- Jeff S. Lundeen
- Charles Bamber
tags: []
categories: []
date: '2012-02-01'
lastmod: 2022-10-12T17:08:13-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:06.626075Z'
publication_types:
- '2'
abstract: Recent work by Lundeen et al. [Nature (London) 474, 188 (2011)] directly
  measured the wave function by weakly measuring a variable followed by a normal (i.e.,
  “strong”) measurement of the complementary variable. We generalize this method to
  mixed states by considering the weak measurement of various products of these observables,
  thereby providing the density matrix an operational definition in terms of a procedure
  for its direct measurement. The method only requires measurements in two bases and
  can be performed in situ, determining the quantum state without destroying it.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.108.070402
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.108.070402
- name: arXiv
  url: https://arxiv.org/abs/1112.5471
- name: PDF
  url: files/publications/lundeen-procedure-2012.pdf
---
