---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Focusing on factorability: space–time coupling in the generation of pure heralded
  single photons'
subtitle: ''
summary: ''
authors:
- Peter J. Mosley
- Jeff S. Lundeen
- Brian J. Smith
- Ian A. Walmsley
tags:
- heralding
- parametric downconversion
- photon
- pure state
categories: []
date: '2009-01-01'
lastmod: 2022-10-12T17:08:15-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.560978Z'
publication_types:
- '2'
abstract: The interference of single heralded photons from multiple parametric downconversion
  sources requires photon pairs in factorable states. Typically, these are selected
  from an ensemble of pairs by narrow filters that remove any exhibiting correlations.
  In order to eliminate these lossy filters, factorable photon pairs free from any
  spatio-temporal correlations must be created directly at each source. This requires
  careful engineering of the group velocity dispersion of the nonlinear crystal in
  which pair generation takes place. Several schemes have been proposed to do this
  in the plane-wave regime, but in a realistic experiment one must also take into
  account the effects of focusing on the two-photon state. Focusing leads to space–time
  coupling between the pump structure and the downconverted pairs that has the potential
  to reduce their factorability, but if carefully managed can actually increase it.
  In this paper, we consider some of the effects of focusing and their consequences
  for pure single photon generation.
publication: '*Journal of Modern Optics*'
doi: 10.1080/09500340802187324
links:
- name: URL
  url: https://doi.org/10.1080/09500340802187324
- name: PDF
  url: files/publications/mosley-focusing-2009.pdf
---
