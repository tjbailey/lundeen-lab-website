---
# Documentation: https://wowchemy.com/docs/managing-content/

title: A double-slit `which-way' experiment on the complementarity–uncertainty debate
subtitle: ''
summary: ''
authors:
- R. Mir
- Jeff S. Lundeen
- M. W. Mitchell
- A. M. Steinberg
- J. L. Garretson
- H. M. Wiseman
tags: []
categories: []
date: '2007-08-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:09.741950Z'
publication_types:
- '2'
abstract: "A which-way measurement in Young's double-slit will destroy the interference\
  \ pattern. Bohr claimed this complementarity between wave- and particle-behaviour\
  \ is enforced by Heisenberg's uncertainty principle: distinguishing two positions\
  \ at a distance s apart transfers a random momentum q ∼ ℏ/s to the particle. This\
  \ claim has been subject to debate: Scully et al (1991 Nature 351 111) asserted\
  \ that in some situations interference can be destroyed with no momentum transfer,\
  \ while Storey et al (1994 Nature 367 626) asserted that Bohr's stance is always\
  \ valid. We address this issue using the experimental technique of weak measurement.\
  \ We measure a distribution for q that spreads well beyond [−ℏ/s, ℏ/s], but nevertheless\
  \ has a variance consistent with zero. This weak-valued momentum-transfer distribution\
  \ Pwv(q) thus reflects both sides of the debate."
publication: '*New Journal of Physics*'
doi: 10.1088/1367-2630/9/8/287
links:
- name: URL
  url: https://doi.org/10.1088/1367-2630/9/8/287
- name: arXiv
  url: https://arxiv.org/abs/0706.3966
- name: PDF
  url: files/publications/mir-double-slit-2007.pdf
---
