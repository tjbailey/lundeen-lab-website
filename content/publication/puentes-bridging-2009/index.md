---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Bridging Particle and Wave Sensitivity in a Configurable Detector of Positive
  Operator-Valued Measures
subtitle: ''
summary: ''
authors:
- Graciana Puentes
- Jeff S. Lundeen
- Matthijs P. A. Branderhorst
- Hendrik B. Coldenstrodt-Ronge
- Brian J. Smith
- Ian A. Walmsley
tags: []
categories: []
date: '2009-02-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.331091Z'
publication_types:
- '2'
abstract: We report an optical detector with tunable positive operator-valued measures.
  The device is based on a combination of weak-field homodyne techniques and photon-number-resolving
  detection. The resulting positive operator-valued measures can be continuously tuned
  from Fock-state projectors to a variety of phase-dependent quantum-state measurements
  by adjusting different system parameters such as local oscillator coupling, amplitude,
  and phase, allowing thus not only detection but also preparation of exotic quantum
  states. Experimental tomographic reconstructions of classical benchmark states are
  presented as a demonstration of the detector capabilities.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.102.080404
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.102.080404
- name: arXiv
  url: https://arxiv.org/abs/0902.1549
- name: PDF
  url: files/publications/puentes-bridging-2009.pdf
---
