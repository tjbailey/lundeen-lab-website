---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental simultaneous readout of the real and imaginary parts of the weak
  value
subtitle: ''
summary: ''
authors:
- A. Hariri
- Davor Curic
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2019-09-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.526171Z'
publication_types:
- '2'
abstract: The weak value, the average result of a weak measurement, has proven useful
  for probing quantum and classical systems. Examples include amplifying small signals,
  investigating quantum paradoxes, and elucidating fundamental quantum phenomena such
  as geometric phase. A key characteristic of the weak value is that it can be complex,
  in contrast to a standard expectation value. However, typically only either the
  real or imaginary component of the weak value is determined in a given experimental
  setup. Weak measurements can be used to, in a sense, simultaneously measure noncommuting
  observables. This principle was used in the direct measurement of the quantum wave
  function. However, the wave function's real and imaginary components, given by a
  weak value, are determined in different setups or on separate ensembles of systems,
  putting the procedure's directness in question. To address these issues, we introduce
  and experimentally demonstrate a general method to simultaneously read out both
  components of the weak value in a single experimental apparatus. In particular,
  we directly measure the polarization state of an ensemble of photons using weak
  measurement. With our method, each photon contributes to both the real and imaginary
  parts of the weak-value average. On a fundamental level, this suggests that the
  full complex weak value is a characteristic of each photon measured.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.100.032119
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.100.032119
- name: arXiv
  url: https://arxiv.org/abs/1906.02263
- name: PDF
  url: files/publications/hariri-experimental-2019.pdf
---
