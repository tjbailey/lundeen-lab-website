---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Quantum State Preparation and Conditional Coherence
subtitle: ''
summary: ''
authors:
- K. J. Resch
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2002-03-01'
lastmod: 2022-10-12T17:08:17-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.330852Z'
publication_types:
- '2'
abstract: It is well known that spontaneous parametric down-conversion can be used
  to probabilistically prepare single-photon states. We have performed an experiment
  in which arbitrary superpositions of zero- and one-photon states can be prepared
  by appropriate postselection. The optical phase, which is meaningful only for superpositions
  of photon number, is related to the relative phase between the zero- and one-photon
  states. Whereas the light from spontaneous parametric down-conversion has an undefined
  phase, we show that this technique collapses one beam to a state of well-defined
  optical phase when a measurement succeeds on the other beam.
publication: '*Physical Review Letters*'
doi: 10.1103/PhysRevLett.88.113601
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevLett.88.113601
- name: PDF
  url: files/publications/resch-quantum-2002.pdf
---
