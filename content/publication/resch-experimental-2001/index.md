---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental observation of nonclassical effects on single-photon detection
  rates
subtitle: ''
summary: ''
authors:
- K. J. Resch
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2001-01-01'
lastmod: 2022-10-12T17:08:18-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:11.819520Z'
publication_types:
- '2'
abstract: It is often asserted that quantum effects can be observed in coincidence
  detection rates or other correlations, but never in the rate of single-photon detection.
  We observe nonclassical interference in a singles rate, thanks to the intrinsic
  nonlinearity of photon counters. This is due to a dependence of the effective detection
  efficiency on the quantum statistics of the light beam. Such measurements of detector
  response to photon pairs promise to shed light on the microscopic aspects of silicon
  photodetectors, and on general issues of quantum measurement and decoherence.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.63.020102
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.63.020102
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0006056
- name: PDF
  url: files/publications/resch-experimental-2001.pdf
---
