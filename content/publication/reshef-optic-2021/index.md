---
# Documentation: https://wowchemy.com/docs/managing-content/

title: An optic to replace space and its application towards ultra-thin imaging systems
subtitle: ''
summary: ''
authors:
- Orad Reshef
- Michael P. DelMastro
- Katherine K. M. Bearne
- Ali H. Alhulaymi
- Lambert Giner
- Robert W. Boyd
- Jeff S. Lundeen
tags:
- Imaging and sensing
- Metamaterials
categories: []
date: '2021-06-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:04.117247Z'
publication_types:
- '2'
abstract: Centuries of effort to improve imaging has focused on perfecting and combining
  lenses to obtain better optical performance and new functionalities. The arrival
  of nanotechnology has brought to this effort engineered surfaces called metalenses,
  which promise to make imaging devices more compact. However, unaddressed by this
  promise is the space between the lenses, which is crucial for image formation but
  takes up by far the most room in imaging systems. Here, we address this issue by
  presenting the concept of and experimentally demonstrating an optical ‘spaceplate’,
  an optic that effectively propagates light for a distance that can be considerably
  longer than the plate thickness. Such an optic would shrink future imaging systems,
  opening the possibility for ultra-thin monolithic cameras. More broadly, a spaceplate
  can be applied to miniaturize important devices that implicitly manipulate the spatial
  profile of light, for example, solar concentrators, collimators for light sources,
  integrated optical components, and spectrometers.
publication: '*Nature Communications*'
doi: 10.1038/s41467-021-23358-8
links:
- name: URL
  url: https://www.nature.com/articles/s41467-021-23358-8
- name: arXiv
  url: https://arxiv.org/abs/2002.06791
- name: PDF
  url: files/publications/reshef-optic-2021.pdf
---
