---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Absolute efficiency estimation of photon-number-resolving detectors using twin
  beams
subtitle: ''
summary: ''
authors:
- A. P. Worsley
- H. B. Coldenstrodt-Ronge
- Jeff S. Lundeen
- P. J. Mosley
- B. J. Smith
- G. Puentes
- N. Thomas-Peter
- I. A. Walmsley
tags: []
categories: []
date: '2009-03-01'
lastmod: 2022-10-12T17:08:14-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:08.055838Z'
publication_types:
- '2'
abstract: A nonclassical light source is used to demonstrate experimentally the absolute
  efficiency calibration of a photon-number-resolving detector. The photon-pair detector
  calibration method developed by Klyshko for single-photon detectors is generalized
  to take advantage of the higher dynamic range and additional information provided
  by photon-number-resolving detectors. This enables the use of brighter twin-beam
  sources including amplified pulse pumped sources, which increases the relevant signal
  and provides measurement redundancy, making the calibration more robust.
publication: '*Optics Express*'
doi: 10.1364/OE.17.004397
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-17-6-4397
- name: arXiv
  url: https://arxiv.org/abs/0906.2182
- name: PDF
  url: files/publications/worsley-absolute-2009.pdf
---
