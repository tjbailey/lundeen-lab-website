---
# Documentation: https://wowchemy.com/docs/managing-content/

title: 'Implementation of nearly arbitrary spatially varying polarization transformations:
  an in-principle lossless approach using spatial light modulators'
subtitle: ''
summary: ''
authors:
- M. T. Runyon
- Codey H. Nacke
- Alicia Sit
- M. Granados-Baez
- Lambert Giner
- Jeff S. Lundeen
tags: []
categories: []
date: '2018-07-01'
lastmod: 2022-10-12T17:08:11-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:05.118515Z'
publication_types:
- '2'
abstract: A fast and automated scheme for general polarization transformations holds
  great value in adaptive optics, quantum information, and virtually all applications
  involving light&#x2013;matter and light&#x2013;light interactions. We present an
  experiment that uses a liquid crystal on silicon spatial light modulator to perform
  polarization transformations on a light field. We experimentally demonstrate the
  point-by-point conversion of uniformly polarized light fields across the wavefront
  to realize arbitrary, spatially varying polarization states. Additionally, we demonstrate
  that a light field with an arbitrary spatially varying polarization can be transformed
  to a spatially invariant (i.e., uniform) polarization.
publication: '*Applied Optics*'
doi: 10.1364/AO.57.005769
links:
- name: URL
  url: https://opg.optica.org/ao/abstract.cfm?uri=ao-57-20-5769
- name: arXiv
  url: https://arxiv.org/abs/1802.05810
- name: PDF
  url: files/publications/runyon-implementation-2018.pdf
---
