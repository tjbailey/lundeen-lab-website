---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Comment on ``Manipulating the frequency-entangled states by an acoustic-optical
  modulator''
subtitle: ''
summary: ''
authors:
- K. J. Resch
- S. H. Myrskog
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2001-10-01'
lastmod: 2022-10-12T17:08:18-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:12.065800Z'
publication_types:
- '2'
abstract: A recent theoretical paper by Shi et al. [Phys. Rev. A 61, 064102 (2000)]
  proposes a scheme for entanglement swapping utilizing acousto-optic modulators without
  requiring a Bell-state measurement. In this Comment, we show that the proposal is
  flawed and no entanglement swapping can occur without measurement.
publication: '*Physical Review A*'
doi: 10.1103/PhysRevA.64.056101
links:
- name: URL
  url: https://link.aps.org/doi/10.1103/PhysRevA.64.056101
- name: PDF
  url: files/publications/resch-comment-2001.pdf
---
