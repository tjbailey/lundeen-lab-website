---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Quantum metrology timing limits of the Hong-Ou-Mandel interferometer and of
  general two-photon measurements
subtitle: ''
summary: ''
authors:
- Kyle M. Jordan
- Raphael A. Abrahao
- Jeff S. Lundeen
tags:
- Quantum Physics (quant-ph)
- Optics (physics.optics)
- 'FOS: Physical sciences'
- 'FOS: Physical sciences'
categories: []
date: '2022-01-01'
lastmod: 2022-10-17T17:52:00-04:00
featured: false
draft: true

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-17T21:51:59.857540Z'
publication_types:
- '0'
abstract: 'We examine the precision limits of Hong-Ou-Mandel (HOM) timing measurements,
  as well as precision limits applying to generalized two-photon measurements. As
  a special case, we consider the use of two-photon measurements using photons with
  variable bandwidths and frequency correlations. When the photon bandwidths are not
  equal, maximizing the measurement precision involves a trade-off between high interference
  visibility and strong frequency anticorrelations, with the optimal precision occuring
  when the photons share non-maximal frequency anticorrelations. We show that a generalized
  measurement has precision limits that are qualitatively similar to those of the
  HOM measurement whenever the generalized measurement is insensitive to the net delay
  of both photons. By examining the performance of states with more general frequency
  distributions, our analysis allows for engineering of the joint spectral amplitude
  for use in realistic situations, in which both photons may not have ideal spectral
  properties. '
publication: '*arXiv*'
doi: 10.48550/ARXIV.2206.11387
links:
- name: arXiv
  url: https://arxiv.org/abs/2206.11387
- name: PDF
  url: files/publications/jordan-quantum-2022.pdf
---
