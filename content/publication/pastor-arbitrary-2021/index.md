---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Arbitrary optical wave evolution with Fourier transforms and phase masks
subtitle: ''
summary: ''
authors:
- Víctor López Pastor
- Jeff S. Lundeen
- Florian Marquardt
tags: []
categories: []
date: '2021-11-01'
lastmod: 2022-10-12T17:08:10-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-17T22:02:34.617197Z'
publication_types:
- '2'
abstract: A large number of applications in classical and quantum photonics require
  the capability of implementing arbitrary linear unitary transformations on a set
  of optical modes. In a seminal work by Reck et al. [Phys. Rev. Lett.73, 58 (1994)10.1103/PhysRevLett.73.58],
  it was shown how to build such multiport universal interferometers with a mesh of
  beam splitters and phase shifters, and this design became the basis for most experimental
  implementations in the last decades. However, the design of Reck et al. is difficult
  to scale up to a large number of modes, which would be required for many applications.
  Here we present a deterministic algorithm that can find an exact and efficient implementation
  of any unitary transformation, using only Fourier transforms and phase masks. Since
  Fourier transforms and phase masks are routinely implemented in several optical
  setups and they do not suffer from some of the scalability issues associated with
  building extensive meshes of beam splitters, we believe that our design can be useful
  for many applications in photonics.
publication: '*Optics Express*'
doi: 10.1364/OE.432787
links:
- name: URL
  url: https://opg.optica.org/oe/abstract.cfm?uri=oe-29-23-38441
- name: arXiv
  url: https://arxiv.org/abs/1912.04721
- name: PDF
  url: files/publications/pastor-arbiatary-2021.pdf
---
