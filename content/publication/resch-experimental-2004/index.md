---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Experimental realization of the quantum box problem
subtitle: ''
summary: ''
authors:
- K. J. Resch
- Jeff S. Lundeen
- A. M. Steinberg
tags: []
categories: []
date: '2004-04-01'
lastmod: 2022-10-12T17:08:16-04:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2022-10-12T23:51:10.577682Z'
publication_types:
- '2'
abstract: The three-box problem is a gedankenexperiment designed to elucidate some
  interesting features of quantum measurement and locality. A particle is prepared
  in a particular superposition of three boxes, and later found in a different (but
  nonorthogonal) superposition. It was predicted that appropriate “weak” measurements
  of particle position in the interval between preparation and post-selection would
  find the particle in two different places, each with certainty. We verify these
  predictions in an optical experiment and address the issues of locality and of negative
  probability.
publication: '*Physics Letters A*'
doi: 10.1016/j.physleta.2004.02.042
links:
- name: URL
  url: https://www.sciencedirect.com/science/article/pii/S0375960104002506
- name: arXiv
  url: https://arxiv.org/abs/quant-ph/0310091
- name: PDF
  url: files/publications/resch-experimental-2004.pdf
---
