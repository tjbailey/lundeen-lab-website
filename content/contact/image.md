---
# An instance of the Blank widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 20

# title:
# subtitle:

# design:
#   columns: '1'
#   background:
#     image: contact.jpg
#     image_darken: 0
#     image_parallax: false
#     image_position: center
#     image_size: cover
#     text_color_light: true
#   spacing:
#     padding: ['20px', '0', '20px', '0']
# advanced:
#   css_class: fullscreen
---

<center>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2800.433127695881!2d-75.68091158365735!3d45.420769579100295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cce0509e607d8bd%3A0xd66ffa525622425d!2s25%20Templeton%20St%2C%20Ottawa%2C%20ON%20K1N%206N5!5e0!3m2!1sen!2sca!4v1665090301123!5m2!1sen!2sca" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

</center>