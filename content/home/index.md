---
# Files in this folder represent a Widget Page (homepage)
type: widget_page
summary: Information about Jeff Lundeen's research group at the University of Ottawa.

# Homepage is headless, other widget pages are not.
headless: true
---
