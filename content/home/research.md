---
# An instance of the Blank widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: blank

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 20

# Section title
title: Research Areas

# Section subtitle
subtitle:

# Section design
design:
  # Use a 1-column layout
  columns: "1"
---

## Measurements
Use Quantum Effects to improve measurements.

## Spaceplate
Compress space with a plate.