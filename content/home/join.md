---
widget: hero
headless: true  # This file represents a page section.

# ... Put Your Section Options Here (title etc.) ...

# Hero image (optional). Enter filename of an image in the assets/media/ folder.
hero_media: ''

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `cta`.
#   Remove a link/note by deleting a cta/note block.
cta:
  url: join
  label: Join the Lab
  icon_pack: fas
  icon: arrow-right
#cta_alt:
#  url: 'https://wowchemy.com'
#  label: View Documentation

advanced:
  css_style: "text-align:center;"

# Note. An optional note to show underneath the links.
cta_note:
  label: ''
---
