---
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://wowchemy.com/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget: blank # See https://wowchemy.com/docs/page-builder/
headless: true # This file represents a page section.
weight: 10 # Order that this section will appear.
#title: 
#hero_media: welcome.jpg
design:
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns: '2'
  # Add custom styles
  css_style:
  css_class:
  background:
    image: mba_crop2.jpg
    text_color_light: true
---

We are an experimental quantum photonics group led by Jeff Lundeen in the Physics Department at the University of Ottawa. We perform research into fundamental issues in quantum physics, quantum radiometry, quantum information with photonic systems, and quantum metrology.